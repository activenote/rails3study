# coding:utf-8

class HelloController < ApplicationController
  def index
    render :text => 'こんにちは、世界！'
  end
  
  def view
    @msghello = 'こんにちは! Railsの世界へ♪'
  end
  
  def list
    @books = Book.all
  end
  
end
